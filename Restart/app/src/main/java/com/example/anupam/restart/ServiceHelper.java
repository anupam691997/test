package com.example.anupam.restart;

import android.app.IntentService;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by anupam on 8/4/17.
 */

public class ServiceHelper extends Service {

    private int cnt=0;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;



    private String[] names;
    private String[] contacts;



    public static Intent getIntentService(Context context,String[] names,String[] contacts)
    {
        Intent intent=new Intent(context,ServiceHelper.class);
        intent.putExtra("names",names);
        intent.putExtra("contacts",contacts);

        return intent;
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }



    @Override
    public void onCreate() {
        super.onCreate();

        sharedPreferences=getSharedPreferences("ayush", Context.MODE_PRIVATE);
        cnt=sharedPreferences.getInt("x",0);

    }




    @Override
    public int onStartCommand(final Intent intent, int flags, final int startId) {


        names=intent.getStringArrayExtra("names");
        contacts=intent.getStringArrayExtra("contacts");




      Runnable runnable=new Runnable() {
          @Override
          public void run() {


              for(int i=0;i<ApplicationHelper.getSqliteHelper().getEntryCount();i++)
              {
                  if(i<cnt)
                      continue;



                  int x=ApplicationHelper.getSqliteHelper().update(names[i]);
                  cnt++;
                  Log.d("Hello",""+x);



                  try {
                      Thread.sleep(2000);
                  } catch (InterruptedException e) {
                      e.printStackTrace();
                  }

              }


              stopSelf();
          }
      };

        Thread thread=new Thread(runnable);
        thread.start();


        return START_REDELIVER_INTENT;
    }


    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);


        editor=sharedPreferences.edit();
        editor.putInt("x",cnt);
        editor.commit();


    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        editor=sharedPreferences.edit();
        cnt=0;
        editor.putInt("x",cnt);
        editor.commit();



    }
}
