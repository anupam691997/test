package com.example.anupam.restart;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private int x;
    private String[] contact;
    private String[] names;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        sharedPreferences=getSharedPreferences("myFile", Context.MODE_PRIVATE);
        x= sharedPreferences.getInt("value",0);

        contact=getResources().getStringArray(R.array.contact);
        names=getResources().getStringArray(R.array.NAME);


        if(x==0)
        {
            for(int i=0;i<contact.length;i++)
            {
               long y= ApplicationHelper.getSqliteHelper().insertData(1,"first_list",names[i],contact[i],0);
                Log.d("Hello",""+y);
            }

            editor=sharedPreferences.edit();
            editor.putInt("value",1);
            editor.commit();
        }

    }


    public void process(View view)
    {
        Intent intent=ServiceHelper.getIntentService(this,names,contact);
        startService(intent);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
