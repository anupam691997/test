package com.example.anupam.restart;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anupam on 8/4/17.
 */

public class SqliteHelper {

    private SqliteHelperInner sqliteHelperInner;
    private SQLiteDatabase sqLiteDatabase;


    public SqliteHelper(Context context)
    {
        sqliteHelperInner=new SqliteHelperInner(context);
        sqLiteDatabase=sqliteHelperInner.getWritableDatabase();

    }


    public long insertData(int list_id,String list_name,String name,String contact,int msg_sent)
    {
        ContentValues contentValues=new ContentValues();
        contentValues.put(SqliteHelperInner.LIST_ID,list_id);
        contentValues.put(SqliteHelperInner.LIST_NAME,list_name);
        contentValues.put(SqliteHelperInner.NAME,name);
        contentValues.put(SqliteHelperInner.CONTACT_NO,contact);
        contentValues.put(SqliteHelperInner.MSG_ID,msg_sent);


        long x=sqLiteDatabase.insert(SqliteHelperInner.TABLE_NAME,null,contentValues);

        return x;
    }



    public int getEntryCount()
    {
        Cursor cursor=sqLiteDatabase.query(SqliteHelperInner.TABLE_NAME,null,null,null,null,null,null);
        return cursor.getCount();
    }


    public int update(String name)
    {
        ContentValues contentValues=new ContentValues();
        contentValues.put(SqliteHelperInner.MSG_ID,1);
        String[] columns={name};
        int x= sqLiteDatabase.update(SqliteHelperInner.TABLE_NAME,contentValues,SqliteHelperInner.NAME+"=?",columns);
        return x;
    }







    public class SqliteHelperInner extends SQLiteOpenHelper
    {
        private static final String DATABASE_NAME="codekamp_connect.db";
        private static final String TABLE_NAME="contact_list_table";
        private static final String UID="_id";
        private static final String LIST_ID="list_id";
        private static final String LIST_NAME="list_name";
        private static final String NAME="name";
        private static final String CONTACT_NO="contact_no";
        private static final String MSG_ID="sms_sent";
        private static final int VERSION=4;


        private static final String CREATE_QUERY="CREATE TABLE "+TABLE_NAME+" ("+UID+" INTEGER PRIMARY KEY AUTOINCREMENT ,"+LIST_ID+" INTEGER(10) ,"+LIST_NAME+" VARCHAR(255) ,"+
                NAME+" VARCHAR(255) ,"+CONTACT_NO+" VARCHAR(255) ,"+MSG_ID+" INTEGER(2));";

        private static final String DROP_QUERY="DROP TABLE IF EXISTS "+TABLE_NAME;




        public SqliteHelperInner(Context context) {
            super(context, DATABASE_NAME, null, VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

            try {
                db.execSQL(CREATE_QUERY);
            }catch (SQLException e)
            {
            }


        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

            try {
                db.execSQL(DROP_QUERY);
            }catch (SQLException e)
            { }

            onCreate(db);

        }
    }


}
